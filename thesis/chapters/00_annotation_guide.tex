\chapter*{Annotation Guidelines}
\label{cha:annotation_guide}

The following annotation guidelines are a combined adaptation of \cite{benikova-etal-2014-nosta}, \cite{ldc_2005}, \cite{muc7_ner}, and \cite{smartdata_anno}.


\section*{Named Entity - Basic Definition}

Names are denominators for a single unique object in the real world. Only noun phrases are considered as named entities. Pronouns and all other phrases can be ignored. 
Phrases only partly containing names (e.g. `deutschlandweit), or adjectives referring to entities (`euklidisch') can be ignored.

\noindent Only one entity type per token span should be annotated. In case of multiple options (e.g. `Charité' could be referred to as organization or as location, same for `Landtag' or `Bundestag'), the context of usage should be considered for the final classification. 

\noindent Named entities can consist of more than one token. The inital token of an entity should be tagged with the B-label of the corresponding entity type. The following tokens belonging to the same entity should be labelled with the I-label of the same type. For 

\noindent Combined named entities including different entity types (e.g. `Wien-Demo') should be tagged separately if possible (Wien[LOC]-Demo[ACTION]), or else tagged as the dominant entity type (here: [ACTION]) to avoid nested entities.

\noindent Whitespace should not be labelled as part of an entity. Token separators such aus /, - or . should be included if they are inside an entity sequence, and excluded if they separate two different entities, or an entity and a non-entity token. Hashtags should be included in the labeled span. 

\noindent Dates and times can be represented by numbers (31.10.2019, 16:00), and strings ('Morgen', 'Uhr'). 

\noindent Emoticons, user mentions and (parts of) urls should not be tagged as named entities. 

\section*{Definition of Different Entity Types}
For annotation, the six entity types person, location, organization, date, time, and action are used. The label set consists of B-tag and  I-tag for each entity type, resulting in a total of twelve labels. 

Table \ref{tab:entity_types} shows the core definitions and examples for the annotated entity types. 

\begin{table}[]
\caption{Definition of entity types for annotation guide}
\label{tab:entity_types}
\centering
\begin{adjustbox}{max width=\textwidth}
\begin{tabular}{|p{0.12\linewidth}|p{0.7\linewidth}|p{0.4\linewidth}|}
\hline
\textbf{Label} & \textbf{Description} & \textbf{Examples} \\
\hline 

PER 
& Named, single individuals, limited to humans. Fictional persons can be included, as long as they’re referred to by name. 
& Karl Lauterbach, Merkel, Harry Potter, Kretschmer, Karl Hilz, Markus
\\
\hline
LOC 
& Names of politically or geographically defined locations, including countries, provinces, regions, cities, districts, addresses including streets, squares, and postal codes, (named) buildings or other man-made structures, shopping centers, parking lots, or restaurants, cardinal points. No metaphorical locations.
& Deutschland, Frankreich, \#BW, Berlin, Bundestag, Restaurant Walliserkanne, Straße des 17. Juni, Neptunbrunnen, Alex, Nollendorfplatz, Norden, Süden, Hellersdorf
 \\ 
\hline
ORG 
& A named corporate, governmental, or other organizational entity: Organization entities can be corporations, agencies, institutions, newspapers, tv/radio channels, other media platforms, political parties, non-governmental organizations, and political groups with a formal or informal organizational structure.
 & ARD, Polizei Berlin, Youtube, Telegram, Amadeu Antonio Stiftung, Querdenken711, Antifa, Bundesregierung, Gesundheitsamt, Französische Polizei, Facebook, Google, Bill Gates Foundation, KSV LiLi, FC St. Pauli, SGD-Ultras, Bündnis21 Hessen, Polizei, Polizeipräsidium Pforzheim
 \\ 
\hline
DATE 
& Absolute or relative date expressions 
& 23.09.2022, 20210930, 3108, Morgen, Heute, 2019, \#Sa2011 \\ 
\hline
TIME 
& Specific, absolute time expressions referring to times of day, and time codes. 
& 13:10, 13:12 Uhr, 16 Uhr, 6:49 \\ 
\hline
ACTION 
& A specific instance of an action or event undertaken to express political beliefs, e.g. demonstrations, vigils, motorcades, walks in public.  
& Kundgebung, Demo, Demonstration, Spaziergang, Aufzug, Aufzug RufderTrommeln \\ 
\hline
\end{tabular}
\end{adjustbox}
\end{table}

\section*{Specifications}

\subsection*{Handling Mixed-language Messages}
Skip mixed-language in which the actual message is non-German language.\\

\noindent\textbf{Examples:}

\begin{itemize}
	\item \textit{We are angry and pissed off - Raus auf die Straße \@Demotermine!}
	\item \textit{Brilliant numbers in Carlisle ! Raus auf die Straßen @Demotermine}
	\item \textit{MASSIMA DIFFUSIONE http://t.me/stopdittatura/1930 Raus auf die Straßen @Demotermine}
\end{itemize}

\subsection*{Handling Incomprehensible Messages}
Skip incomprehensible messages.\\

\noindent\textbf{Example:} \textit{``Sterne-Restaurant Walliserkanne . Naudreck . Bersets faule Tricks . Uniarzt panikfrei . Herzmuskel .  Neuer Beitrag \#Walliserkanne Raus auf die Straßen \@Demotermine! Übersicht / Overview''}

\subsection*{Handling Short Messages}
Skip short messages which do not include at least two named entities.\\ 

\noindent\textbf{Examples:}
\begin{itemize}
	\item \textit{``\@AnwaelteFuerAufklaerung \@Demotermine''}
	\item \textit{``Egal wo ihr diesen , oder die kommenden Samstage seid ! \@RheinCandleLight \@Demotermine''}
\end{itemize}

\subsection*{Handling Punctuation}
Punctutaion should only be included in the tag if they appear in the middle of sequence representing an entity.\\

\noindent\textbf{Example}: \textit{Donnerstag[B-DATE] ,[I-DATE] 17.04.2020[I-DATE]}

\subsubsection*{Hashtags}
If a hashtag is part of a named entity, it should be included in the tag.\\

\noindent\textbf{Examples:}
\begin{itemize}
	\item \#Berlin[B-ORG]
	\item \#BW[B-ORG]
	\item \#CH[B-ORG]
	\item \#Hardy[B-PER] Groeneveld[I-PER]
	\item \#Mutigmacher[B-ORG]
\end{itemize}

\noindent If a hashtag is part of a sequence combining two named entities, it should be included in the tag for the first entity.\\

\textbf{Example:} \#be[B-LOC]2908[B-DATE]
	
\section*{Detailed Annotation Instructions for Entity Types}

\subsection*{PERSON}
Person names can consist of given name and last name, or only first/last name. Titles (e.g. Dr.) and forms of address (Herr, Frau) should not be annotated.  

\noindent\textbf{Examples:}
\begin{itemize}
	\item \textit{Scholz[B-PER]}
	\item \textit{Carolin[B-PER] Matthie[I-PER]}
	\item \textit{Merkel[B-PER]}
	\item \textit{Anselm[B-PER] Lenz[I-PER]}
	\item \textit{Karl[B-PER]}
\end{itemize}

\subsection*{LOCATION}
Do not label derived location words (e.g. \textit{Kreuzberger Nächte} as location. 

If a a sequence contains a number of tokens representing a location entity (e.g. an address), use the B-tag for the initial token and the I-tag for all succeeding tokens belonging to the same entity, including special characters such as , - . or /. The sequence should be long enough to create a representation of an entity which can be understood without the succeeding tokens.\\
 
\noindent\textbf{Examples:}
\begin{itemize}
	\item \textit{Straße[B-LOC] des[I-LOC] 17.[I-LOC] Juni[I-LOC]}
	\item \textit{Bad[B-LOC] Oldesloe[I-LOC]}
	\item \textit{Elbe[B-LOC] Elster[I-LOC]}
	\item \textit{Bremen[B-LOC]-Vegesack[I-LOC]}
	\item \textit{Lutherstadt[B-LOC] Wittenberg[I-LOC]}
	\item \textit{Landeplatz[B-LOC] Grießkirchen[I-LOC]}
	\item \textit{Google[B-LOC] center[I-LOC]}
	\item \textit{Stuttgarter[B-LOC] Landtag[I-LOC]}
	\item NOTE: \textit{Berlin[B-LOC] Neptunbrunnen[B-LOC]}
\end{itemize}


\noindent If the sequence contains a specification of the location, include the specification using the I-tags. 

\noindent\textbf{Examples:}
\begin{itemize}
	\item \textit{Königstraße[B-LOC] Richtung[I-LOC] HBF[I-LOC]}
	\item \textit{Bernauer[B-LOC] Str[I-LOC] Richtung[I-LOC] Süden[I-LOC]}
	\item \textit{Marktoberdorf[B-LOC]-Kaufbeuren[I-LOC]-Neugablonz[I-LOC]}
	\item \textit{Bremen[B-LOC]-Vegesack[I-LOC]}
	\item \textit{D-67454[B-LOC] Haßloch[I-LOC]}
\end{itemize}


\subsection*{ORGANIZATION}
If a a sequence contains a number of tokens representing an organization entity, use the B-tag for the initial token and the I-tag for all succeeding tokens belonging to the same entity, including special characters such as , - . or /. The sequence should be long enough to create a representation of an entity which can be understood without the succeeding tokens.\\

\noindent\textbf{Examples:}
\begin{itemize}
	\item \textit{Bundesregierung[B-ORG]}
	\item \textit{Gesundheitsamt[B-ORG]}
	\item \textit{Französische[B-ORG] Polizei[I-ORG]}
	\item \textit{Google[B-ORG]}
	\item \textit{Bill[B-ORG]  Gates[I-ORG] Foundation[I-ORG]}
	\item \textit{KSV[B-ORG] LiLi[I-ORG]}
	\item \textit{FC[B-ORG] St.[I-ORG] Pauli[I-ORG]}
	\item \textit{Bündnis21[B-ORG] Hessen[I-ORG]}
	\item \textit{Polizeipräsidium[B-ORG] Pforzheim[I-ORG]}
\end{itemize}


\subsection*{TIME}
Time expressions can include specific times of day and time codes (usually appearing after URLs. Sequences denoting a time span / duration should be annotated with the B-tag for the initial token and I-tags for the succeeding tokens.\\ 

\noindent\textbf{Examples.}
\begin{itemize}
	\item \textit{15:00[B-TIME]}
	\item \textit{17:00[B-TIME] Uhr[I-TIME]}
	\item \textit{www.example.com[O] 00:30[B-TIME]}
	\item \textit{Mittag[B-DATE]}
	\item \textit{15:30[B-TIME] -[I-TIME] 19:30[I-TIME]}
\end{itemize}

\subsection*{DATE}
Date expressions can be numeric values or strings. Sequences denoting a date spanshould be annotated with the B-tag for the initial token and I-tags for the succeeding tokens.\\ 

\noindent\textbf{Examples:}
\begin{itemize}
	\item \textit{2019[B-DATE]}
	\item \textit{14.12.21[B-DATE]}
	\item \textit{2.7.21[B-DATE]}
	\item \textit{31. [B-DATE] JULI[I-DATE]}
	\item \textit{01122020[B-DATE]}
	\item \textit{Pfingstmontag[B-DATE]}
	\item \textit{HEUTE[B-DATE]}
	\item \textit{3108[B-DATE]}
	\item \textit{201905[B-DATE]}
	\item \textit{2019/03/05[B-DATE]}
	\item \textit{28. [B-DATE]+ [I-DATE] 29.8. [I-DATE]}
	\item \textit{17042021[B-DATE] –[I-DATE] 21042021[I-DATE]}
\end{itemize}


\subsection*{ACTION}
Actions represent unique real-world events or actions. They must not necessarily be named, but be identifiable as unique object in the context in which they are mentioned. Also, the type of action should be expressed.\\

\noindent\textbf{Examples:}
\begin{itemize}
	\item \textit{Angriff[B-ACTION]}
	\item \textit{Demo-B}
	\item \textit{Autokorso[B-ACTION]}
	\item \textit{Offenes[B-ACTION] Treffen[I-ACTION]}
	\item \textit{Aufzug[B-ACTION] RufderTrommeln[I-ACTION]}
	\item \textit{Zug[B-ACTION]}
	\item \textit{Plakate[B-ACTION]}
	\item \textit{Infostände[B-ACTION]}
	\item \textit{Informationstouren[B-ACTION]}
	\item \textit{Anti[B-ACTION]-Corona[I-ACTION]-Proteste[I-ACTION]}
	\item \textit{Protestzug[B-ACTION]}
	\item \textit{Spaziergänge[B-ACTION]}
	\item \textit{Querdenker[B-ORG]-Demo[B-ACTION]}
	\item \textit{Korso[B-ACTION] Nord[I-ACTION]}
	\item \textit{Gedenken[B-ACTION]}
	\item \textit{Aktionswoche[B-ACTION]}
	\item \textit{Wahlkampf[B-ACTION]}
	\item \textit{Live-Stream[B-ACTION]}
\end{itemize}



