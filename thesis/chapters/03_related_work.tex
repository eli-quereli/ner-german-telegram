\chapter{Related Work}
\label{cha:related_work}

To the best of my knowledge, there are no approaches using \gls{ner} for monitoring the COVID-19 protests in Germany, or any other works focussing on \gls{ner} in German-language texts from messenger services. 

There are, however, a few works using \gls{ner} for analyzing (English-language) discourses in the context of the COVID-19 pandemic: These approaches rely on \gls{ner} as a component for analyzing conspiracy narratives and the corresponding communities, using blog texts and news articles (\cite{tangherlini_automated_2020}), or comments from the imageboard 4chan (\cite{shahsavari_conspiracy_2020}). 

%There is, however, prior research on \gls{ner} in general, partly covering German-languag texts, as well as on \gls{ner} on social media texts. A few studies use \gls{ner} to analyze conspiracy narratives in the context of the COVID-19 pandemic.  
Furthermore, there is a number of shared tasks and corresponding datasets which contributed to the development of German-language \gls{ner} and \gls{ner} in social media text data. For \gls{ner} in social media texts, I will also present a selection of experimental studies which formed the basis for later shared tasks. 

\section{Shared Tasks on German-Language NER}
A first important cornerstone for German-language \gls{ner} was the \gls{conll} for English and German-language \gls{ner} in 2003. The task focused on the entity types person, location, organization, and miscellaneous. The provided German dataset consists of 909 newspaper articles from the `Frankfurter Rundschau'. 
The dataset was annotated by non-native speakers at the University of Antwerp which is assumed to have led to annotation inconsistencies.\footnote{Some authors, e.g. \cite{schweter2020flert} mention a revised edition, but I was not able to find further details or the mentioned resource itself.}
The best participating system combined \glspl{mem} and \glspl{hmm}, and achieved an F1 score of 72.41 (cf. \cite{tjong_kim_sang_introduction_2003})

Another important event was the shared \gls{ner} task 
announced in 2014 by GermEval, a series of shared tasks on Natural Language Processing for German. The provided dataset consists of  31,000 sentences sampled from Wikipedia articles and online news, and includes fine-grained annotations for entities of type person, organization, location, and other. The Germeval 2014 corpus covers both coarse-grained (first level) and fine-grained (second level) entities, including nested and derived entities. While the corpus consists of online resources, it does not contain noisy user-generated texts. 
The best system achieved an F1 score of 79.08 for first-level named entities, using character query-based features in order to find sequences that are characteristic for named entities, and vocabulary clusters for semantic generalization (cf. \cite{benikova-etal-2014-nosta}). 

The two corpora represent standard datasets for German-language \gls{ner} and are used until today to train and evaluate state of the art NER models. 

\section{Experimental Studies and Shared Tasks on Social Media NER}
\label{sec:shared_tasks_sm}
While the first shared tasks on \gls{ner} focussed on newswire articles, later work began to explore \gls{ner} in user-generated texts from social media.

\citeauthor{ritter_2011} were among the first to explore \gls{ner} in social media texts. In 2011, the authors conducted an experimental study, using a random sample of 2,400 English-language tweets annotated with ten different entity types\footnote{Including person, geo-location, company, product, facility, tv-show, movie, sportsteam, band, and other (cf. \cite{ritter_2011})}. 
The authors use a discriminate approach for segmentation, and a weakly supervised approach for classification, relying on topic modeling and a large knowledge base. The system achieved an F1 score of 0.66 when applied to ten entity types. When constrained to the three standard entity types person, location, and organization, the F1 score increased to 0.75, with 0.83 for person, 0.74 for location, and 0.66 for organization (cf. \cite{ritter_2011}). 

Another study conducted in 2015 by \citeauthor{derczynski_analysis_2015} used three different English-language datasets including the \citeauthor{ritter_2011} corpus. The results confirm the rather low performance of by then existing \gls{ner} systems on noisy user-generated data: The evaluated systems achieve F1 scores between 40.27, 51.49, and 77.18 on the different datasets (cf. \cite{derczynski_analysis_2015}). In line with \cite{ritter_2011}, the authors identify unreliable capitalization, typographic errors, abbreviations, and lack of context due to the shortness of texts as main challenges. 

Also in 2015, \citeauthor{baldwin_shared_2015} organized a shared task for named entity recognition on social media data, namely tweets from Twitter in the context of the \gls{wnut}, using the Ritter corpus. The best participating system combined the Stanford NER tagger with entity linking via a knowledge base. It achieved an F1 score of 56.41 (cf. \cite{baldwin_shared_2015}), which is $\sim$22.7 lower than the best F1 score at GermEval 2014 (79.08, cf. \cite{benikova2014germeval}). 
The \gls{wnut} series continued to address the task of \gls{ner} in subsequent years: Tasks were organized on named entity recognition in tweets (cf.\cite{strauss_results_2016}), and novel and emerging entity recognition (cf. \cite{derczynski_results_2017}), both using the Ritter corpus.\footnote{In 2017 the Ritter corpus was used as training set, extended by $\sim$1.000 manually annotated documents from different online forums and platforms as development set, and $\sim$ 1.300 documents manually annotated Youtube comments as test set}. The achieved F1 scores remained rather low with best F1=52.41 in 2016, and best F1=41.86 in 2017. 

Research on \gls{ner} for social media texts is almost completely focused on English-language texts: In 2017, \citeauthor{bhoi_2017} analyzed seven different systems for \gls{ner} in social media text, out of which five were exclusively English (cf. \cite{bhoi_2017}). The only system supporting German language was the Stanford NER tagger, which was trained on newswire, and not on social media texts though. The authors compare system performance for tweets and microblog content. They report F1=68.6 as best result for tweets, and F1=88.9 as best score for the more formal microblog content, concluding that `state-of -the-art NER methods are giving unsatisfactory results in that informal and noisy content of social media text' (\cite{bhoi_2017}).

Even though \gls{ner} on social media texts remained a challenging task regarding the results achieved at the \gls{wnut} shared tasks, its organizers shifted its focus to other tasks and domains in subsequent years. 

In parallel, larger human-annotated social media \gls{ner} datasets have been created (cf. \cite{derczynski-etal-2016-broad}). However, there is still a lack of such corpora for languages other than English. And while the introduction of transformer models in recent years has outperformed prior approaches in many \gls{nlp} tasks including \gls{ner}, the too are commonly fine-tuned and evaluated on the standard \gls{conll}2003 and GermEval2014 corpora. 