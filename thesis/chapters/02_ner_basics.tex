\chapter{Named Entity Recognition: An Introduction}
\label{cha:ner_intro}

\gls{ner} is the task of extracting named entities, for example names of persons, locations, or organizations, from text. The task is situated within the field of \gls{nlp}, and often an an important subtask of \gls{ie}. 

The origins of \gls{ie} date back to the late 1970s, and its development was further accelerated in the late 1980s through funding by the US government for activities to evaluate \gls{ie} technology. Organized by the \gls{darpa} the first \gls{muc} took place in 1987, providing standard datasets for international researchers to compete on. The \gls{muc} and was held seven times between 1987 and 1997. 
 
The first \gls{ner} task at the \gls{muc} was introduced in 1996 and aimed at developing domain-independent systems for recognizing entities of types person, organization, and location, but also numerical expressions such as time, currency, and percentage (cf. \cite{grishman_message_1996}) in newspaper articles. In the following year, the conference organizers developed annotation guidelines which were used as a basis for several later \gls{ner} tasks (cf. \cite{muc7_ner}). 
Later activities such as the \gls{ace} and the \gls{tac_kbp} shifted the focus to the extraction of more fine-grained entities, entity relations, events, entity attribute extraction and entity linking. The provided datasets consisted of broader news data, covering political and international events, and later of open domain data, mainly from webpages (cf. \cite[229]{zong_information_2021}). 

\section{NER as a Sequence Labeling Task}
NER is usually regarded as a sequence labeling task: To each word $x_i$ in an input sequence, a label $y_i$ is assigned, resulting in an output sequence $Y$ which has the same length as the input sequence $X$ (cf. \cite[148]{jurafsky_2021}).  
 
Since named entities often consist of more than one token, the task actually requires labeling spans of texts, which makes it necessary to determine the correct boundaries of an entity in a text, and assign the correct label to each token in a detected span (cf. \cite[154-155]{jurafsky_2021}). 

\gls{ner} thus involves the following two subtasks (cf.\cite[229]{zong_information_2021}): \\ \newline
\textbf{1. Entity detection} to detect whether a given string in a text document is an entity, including boundary detection.\\ \newline
\textbf{2. Entity classification} to assign a specific category to the detected entity. 


\section{Standard Named Entity Types}
The types of entities extracted via an \gls{ner} system differ depending on the domain and purpose of an application: For biomedical \gls{ner} systems for example, the entity types of interest may be names of proteins or diseases. There are, however, some standard entity types commonly used by general \gls{ner} systems. These are \verb|PERSON|, \verb|LOCATION|, \verb|ORGANIZATION|, and \verb|OTH|er or \verb|MISC|ellaneous named entities (cf.\cite[229]{zong_information_2021}).

As we have seen above, earlier \gls{ner} approaches also included numerical expressions such as time, date, currency/quantity, and percentage. Considering that a named entity is commonly defined as a single, unique real-world object, these numerical entities are not named entities in a narrow sense. They can, however, be useful categories depending on the purpose of an application (cf. \cite[386]{aggarwal_machine_2018}).

\section{The BIO Tagging Scheme}
\label{bio_tagging}
While some of the early \gls{ner} approaches were exclusively rule-based, contemporary approaches usually rely on machine learning models trained on labeled datasets. For creating such corpora, an appropriate label set needs to be defined. A widely used tagging scheme is the BIO label set (cf. \cite{ramshaw-marcus-1995-text}). Here, the B-tag is used to label the token representing the beginning of an entity (e.g. \verb|B-PERSON)|, the I-tag is assigned to all of the following (intermediate or final) tokens of the respective entity (e.g. \verb|I-PERSON|). All tokens who are located outside of named entity spans are labeled with the \verb|O| tag (cf. \cite[231]{zong_information_2021}). The corresponding tag set thus consists of two labels per entity and the \verb|O| tag, resulting in a total of $2n + 1$ tags, with $n = number\:of\:entity\:types$. 

\section{Evaluation of NER Systems}
\label{evaluation_metrics}

Like other \gls{nlp} tasks, \gls{ner} systems are usually systematically evaluated. This evaluation can be done in different stages of the development process, during deployment, and in production. 

Evaluation in production usually involves application-specific business metrics, but also the involvement of stakeholders, and sometimes even end-users, which makes it rather expensive. 
Evaluation during development and deployment is therefore usually based on standardized machine learning metrics. This intrinsic evaluation is less cost-effective since it can be done by the machine learning engineers themselves. Furthermore, it allows for adjustments and improvements in the earlier stages of the development process (cf. \cite[71]{vajjala_practical_2020}). 

For supervised machine learning systems, the evaluation usually measures  the model's performance on unseen data, called the test set (cf. \cite[35]{jurafsky_2021}). 
This test set is excluded from the training process and then used to evaluate the model. This is done by comparing the predictions of the model against the labels in the test set. (cf. \cite[241]{zong_information_2021}). The specific metrics used for intrinsic evaluation differ depending on the specific task. For \gls{ner}, the standard metrics are F1, precision, and recall. 

\subsection{F1, Precision, and Recall}

Precision and recall evaluate the model's predictions by taking into account the number of entities correctly recognized by the model as named entities (true positives, TP), the number of entities falsely tagged as entities by the model (false positives, FP), and the number of named entities not detected by the model (false negatives, FN). Precision and recall are defined as follows ((cf. \cite[241]{zong_information_2021}): 

$$ precision = \frac {TP} {TP + FP} * 100 $$
$$ recall = \frac {TP}  {TP + FN} * 100 $$

Precision thus measures the percentage of correctly identified entities out of all entities detected by the system, i.e. how many correct predictions the model makes in its positive predictions. Recall measures the percentage of items tagged as named entities in the test set which are correctly recognized by the system. This allows to how many named entities are missed by the model. 

The F1 score combines these two scores by computing a harmonic mean (cf. \cite[241]{zong_information_2021}):  
$$ F1 = \frac {2 * precision * recall} {precision + recall} $$

F1 is the commonly used metric in shared \gls{ner} tasks (cf. \cite[241]{zong_information_2021}).

Compared to other sequence labeling tasks, \gls{ner} poses certain challenges for evaluation: The fact that named entities can consist of multiple tokens brings up a segmentation problem. For evaluation this means for example that a system which recognizes only one token of a person's name consisting of two tokens will cause two errors: A false positive for tagging the second token as \verb|O|, and a false negative for not labeling the second token as \verb|I-PERSON|) (cf. \cite[167]{jurafsky_2021}). 

Past shared tasks for \gls{ner} have applied different strategies to evaluate the participating systems. Earlier tasks relied on relaxed metrics, considering a prediction as correct if the correct label was assigned, regardless of the correct boundaries, or if the correct text boundaries were detected, regardless of the correct label (cf. \cite{yadav_survey_2019}). Later, strict metrics were introduced in which a prediction was only considered correct if both the predicted label and boundaries of an entity matched the true label and boundaries (cf. \cite{yadav_survey_2019}).

\subsection{Accuracy}
Accuracy is another common machine learning metric. It measures the percentage of items correctly classified by the system: 

$$ accuracy = \frac {TP + TN} {TP + FP + TN + FN} *100 $$
\\
However, accuracy is not suitable for classification tasks on highly imbalanced datasets: In an \gls{ner} dataset, the majority of tokens will be tagged as \verb|O|. A classifier which would predict this majority class for all tokens without having learned anything from the training data would still achieve a very high accuracy (e.g. 90\% accuracy for a dataset containing 90\% \verb|O| tags) (cf. \cite[65-66]{jurafsky_2021}). This is why accuracy is not a suitable metric for \gls{ner}. 


\section{General Challenges for NER}
As an \gls{nlp} task, \gls{ner} faces some general challenges resulting from the characteristics of human language.

\textbf{Ambiguity}, i.e. uncertainty or inexactness of meaning, is inherent to most human languages. Resolving the ambiguity of words or phrases usually requires knowledge of the context in which they appear.  For example, the word `Bill' might refer to a persons first name, but also by a synonym for ìnvoice' when located at the beginning of the sentence.
Making computer-based systems take into account and `understand' \textbf{context} is one of the major challenges for \gls{ner} (cf.  \cite[386]{aggarwal_machine_2018}).  

Humans furthermore constantly use \textbf{common knowledge} in their communication, meaning that facts are assumed to be known and thus not expressed explicitly in statements. 
While every language is constituted and shaped by rules, it is far from being exclusively rule-driven. Rather, languages are \textbf{creative} and heterogenous, characterized by various styles, genres, and dialects.(cf. \cite[14]{vajjala_practical_2020}). Moreover, language evolves over time, in interaction with social and cultural processes, and the development of communication technologies such as social media. Existing datasets thus might not reflect new language phenomena such as hashtags.
For named entities, this means that the set of known entities is never constant nor complete (\cite[386]{aggarwal_machine_2018}). 

Another challenge are \textbf{abbreviations} referring to multiple instances of the same named entity, such as `U.S.', `USA' or `United States of America', all referring to the same geopolitical entity. Entity disambiguation is thus usually an important follow-up task for \gls{ner} (cf.\cite[387]{aggarwal_machine_2018}). 

\textbf{Capitalization} conventions vary widely across languages: In English-language texts for example, capitalization usually indicates proper names and can thus be used as a feature for \gls{ner}. In German by contrast, all nouns are capitalized, which makes the feature less helpful (cf. \cite{benikova-etal-2014-nosta}).

\textbf{Diversity across languages} makes it difficult to transfer a language-specific application to other languages. 
Building language-agnostic solutions is a very complex matter, while building separate solutions for each language is labor- and time-intensive (cf. \cite[14]{vajjala_practical_2020}).

\textbf{Imbalanced resources for different languages} limit the diversity both of research and applications. In fact, most \gls{nlp} technologies are still designed for English or other European languages (cf. \cite{singh_natural_2008}). Correspondingly, a large number of datasets and libraries exists for these high-resource languages. Low-resource languages are usually significantly `less studied, resource scarce, less computerized' (\cite{magueresse_low-resource_2020}). This imbalance is also reflected in existing datasets for \gls{ner}. 

\section{Challenges for NER in User-Generated Texts}
\label{sec:challenges_user_generated_texts}
While the above-mentioned challenges apply in general for all \gls{ner} approaches, the characteristics of user-generated texts from online domains further complicate the task:

Social media texts are often \textbf{more informal and noisy} than newspaper articles, they contain misspellings or domain-specific expressions such as hashtags. Figure \ref{fig:noisy_texts} illustrates the noisy character of tweets as an example. 

Moreover, user-generated texts from social media are often of \textbf{shorter length} than e.g. newspaper articles which might result in a lack of context (cf. \cite{ritter_2011}).

The \textbf{wide variety of capitalization styles} are another challenge: Some users might exclusively use lowercasing, while some texts are completely in uppercase, or follow an inconsistent capitalization strategy (cf. \cite{ritter_2011}). As \cite{ritter_2011} point out, earlier news-trained \gls{ner} systems seem to rely heavily on capitalization which limits their applicability to user-generated social media texts.

Also, these texts usually contain more \textbf{\gls{oov} words} than e.g. news articles. This is partly due to misspellings, but also stems from platform specific codes and styles: The frequent occurrence of \textbf{special characters} such as \# for hashtags or @ for user mentions poses challenges because they cannot easily be handled during annotation and training. User-generated text in social media also often includes a lot of \textbf{lexical variations}. For example, \citeauthor{ritter_2011} collected more than fifty variations of the word `tomorrow' from their Twitter corpus, as shown in figure \ref{fig:lex_var}.

\begin{figure}[H]
	\begin{minipage}[t]{0.5\linewidth}
		\includegraphics[width=\linewidth]{images/noisy_texts_ritter.png}
		\caption{Examples of noisy text in tweets, taken from \cite{ritter_2011}}
 		\label{fig:noisy_texts}
 	\end{minipage}
 	\hfill
	\begin{minipage}[t]{0.45\linewidth}
\includegraphics[width=\linewidth]{images/lexical_variations_ritter.png}
	\caption{Lexical variations of the word `tomorrow' in tweets, taken from \cite{ritter_2011}}
    \label{fig:lex_var}
	\end{minipage}
\end{figure}

Last but not least, social media texts often \textbf{mixed-language texts}, making it harder for language-specific models to accurately determine named entities in them.

Given these differences between social media texts and more formal genres, \gls{ner} on social media needs to handle the problem of \textbf{domain adaptation}. Most of the existing \gls{ner} systems are in fact trained on a small selection of standard corpora mainly consisting of newswire texts, and earlier studies found that applying such a system to social media texts results in a drop of performance (cf. \cite{ritter_2011}). 

