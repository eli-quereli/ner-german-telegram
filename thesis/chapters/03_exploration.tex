\chapter{SpaCy and Flair: A Qualitative Exploration of Existing Solutions}
\label{cha:exploration}

Most of the existing \gls{ner} models are limited to the four common entity types person, location, organization, and other/miscellaneous. To the best of my knowledge, there are no existing models supporting the detection of the entity types time, date, and action. An exception is the English-language spaCy model which supports at least time and date recognition. 

To explore the applicability of existing models for my purpose, I selected some sample messages from a German-language Telegram channel and qualitatively evaluated the results of two German-language \gls{ner} models. 
Since the English spaCy model supports more entity types, and English models are known to be well-resourced, I used machine translation to translate the sample texts into English and explore the results of the English models as well.\footnote{For implementation details, see \url{https://github.com/eli-quereli/ner-german-telegram/tree/main/src/notebooks/3_exploration/spacy_flair_bert_first_experiments}.}

All tested models were selected from the library spaCy and the framework FLAIR. The models were chosen due to their easy applicability, allowing for fast exploration and analysis, and because both of them support German- and English-language named entity recognition.

I am aware of the fact that the observations presented here cannot be generalized without quantitative evaluation on a larger sample. 

\section{Example Data}
I randomly selected ten example messages from the mainly German-language Telegram channel `Demotermine'. Prior qualitative exploration of sample messages indicate that this channel contains a lot of messages mobilizing for various kinds of protests against the COVID-19 containment measures in Germany, making it a valuable resource for my experiments.

\section{German-language NER with spaCy and Flair}
My first analysis focusses on results for German-language sample texts, using the medium-size German-language spaCy model and the standard German-language Flair \gls{ner} model. 

\subsection{The German-language spaCy model}
spaCy is a free open-source library for \gls{nlp}. The library is extensively documented and thus serves as an easy starting point for beginners, while also claiming to be designed for use in production.
spaCy supports more than 66 languages (including German) and a variety of \gls{nlp} tasks such as part-of-speech tagging, text classification, and \gls{ner} (cf. \cite{spacy_facts}). 
The medium-size German model supports to the common entity types person, location, organization, and miscellaneous (cf. \cite{spacy_german_md}). 

The central component of the model's \gls{ner} pipeline is spaCy's Entity Recognizer based on transition-based algorithm which assumes that the most relevant information about an entity is close to its first token (cf. \cite{spacy_entity_recognizer}). The underlying TransitionBasedParser is a neural network state prediction model which embodies `an approach to structured prediction where the task of predicting the structure is mapped to a series of state transitions.' (\cite{spacy_transition_based_parser}). 

The model was trained on the WIKINER corpus, a free, multilingual corpus created from Wikipedia articles for which the annotations were created automatically (cf. \cite{nothman_learning_2013}). The corpus is thus only silver standard (compared to gold standard corpora based on human annotations). The model achieved an F1 score of 84.00 (cf. \cite{spacy_german_md}).

\subsection{The German-language Flair Model}
\label{sec:flair_de}
The FLAIR framework is an \gls{nlp} library developed to `facilitate training and distribution of
state-of-the-art sequence labeling, text classification and language models' (\cite{akbik2019flair}). The overall aim is to provide an interface for the easy use of different word and document embeddings (cf. \cite{akbik2019flair}).

The German-language Flair model for \gls{ner} uses a combination of GloVe embeddings (cf. \cite{pennington2014glove}) and Flair embeddings (cf. \cite{akbik2018coling}) to create word embeddings. Flair embeddings create `contextual string embeddings' (\cite{akbik2018coling}) which generate different embeddings for the same word depending on its context in a document (cf. \cite{akbik2018coling}).
The word embeddings are passed to a \gls{bilstm}-\gls{crf} sequence labeling model for the \gls{ner} downstream task (cf. \cite{akbik2018coling}). 

I used Flair's German standard 4-class \gls{ner} model, supporting the entity types person, location, organization, and other. It was trained on the \gls{conll} 2003 corpus, reaching an F1 score of 87.94 (cf. \cite{flair_ner_german_standard_model}).                

\subsection{Results}

Table \ref{tab:detect_ents_de} shows the results for both models out of which the following observations can be made: 

Overall, the spaCy model yields more false positives: It falsely tags the sequences `Endkundgebung' (final rally), `GEHT DEMONSTRIEREN' (go demonstrate), `Dresdner \&', `Videos', `Berliner', `Versammlungsfre' (freedom of assembly, truncated), `Einfach' (simple/simply), `Der Mann mit dem Schild' (the man with the sign), and `Einsatzwägen' (patrol cars). 

The Flair model only falsely tags `FU' and `Demo-Berlin'. The sequence `FU' does not exist in the original text, but is apparently a result of the model's pre-processing. Tagging `FU' as organization is plausible however, as is tagging `Demo-Berlin' as location since the sequence contains `Berlin'. 

The spaCy model sometimes correctly detects a named entity, but then assigns the wrong tag to it: `Blankenfelde Mahlow' represents a location, but is tagged as person. 

The spaCy model does not detect `Freiburg' in the first message and thus yields a false negative, while the Flair model correctly detects the location mention. 

Somewhat surprisingly, spaCy recognizes parts of the sequence `Platz der alten Synagoge` (square of the old synagogue) as a location which is missed by the Flair model. 

Even though the sample is too small for generalization, my observations casts strong doubts on the claim that spaCy offers a production-ready \gls{ner} solution. I assume that the differences in performance result both from the different model architectures and the quality of the respective training corpora. 

\begin{table}[htp]
\caption{spaCy vs. Flair: Detected entities for German-language example messages}
\label{tab:detect_ents_de}
\centering
\begin{adjustbox}{max width=\textwidth}
\begin{tabular}{|p{0.04\linewidth}|p{0.55\linewidth}|p{0.36\linewidth}|p{0.3\linewidth}|}
\hline
\textbf{No.} & \textbf{Text} & \textbf{spaCy/de\_core\_news\_md} & \textbf{flair/ner-german} \\
\hline 
1 
& Freiburg Aufzug mit Endkundgebung am Platz der altenSynagoge . Live Musik ! Details 19062021 Raus auf die Straße 
& Endkundgebung[LOC], alten Synagoge[LOC]
& Freiburg[LOC] \\
\hline
2 
& GEHT DEMONSTRIEREN ! - Message von Elijah Tee an die Dresdner \& anderen Demonstranten in Deutschland Morgen am 13.06. um 16.30 Uhr und bundesweit überall , wo es Mitdenker gibt . - Bilder und Videos hochladen - Diskussion Fahren oder Mitfahren
& GEHT DEMONSTRIEREN [ORG], Elijah Tee[PER], Dresdner \&[LOC], Deutschland[LOC], Videos[MISC]
& Elijah Tee[PER], Deutschland[LOC] \\ 
\hline
3 
& 15831 Blankenfelde Mahlow Berliner Speckgürtel Ausfahrt Lichtenrade am 14.03.2021 14032021 Raus auf die Straße
& Blankenfelde Mahlow[PER], Berliner[MISC]
& Blankenfelde Mahlow[LOC], Lichtenrade[LOC]\\ 
\hline
4 
& 2021 10 11 Sven Liebich fragt Ministerpräsident Haseloff nach dessen Verständnis 
von Versammlungsfre - Sven Liebich 0:23 Raus auf die Straßen  \"Ubersicht / Overview 
& Sven Liebich[PER], Haseloff[PER], Versammlungsfre[PER], Sven Liebich[PER]] 
& Sven Liebich[PER, Haseloff[PER], Sven Liebich[PER]\\ 
\hline
5 
& F\"UR ALLE DEMOKRATEN   GEFAHR : Die Polizei blockiert den Stern !   Deswegen laufen die Menschen vom Ernst-Reuther-Platz nach Südosten zum Breitscheitplatz und dort weiter auf den Kudamm ! ! Schließt euch an : Neue Kantstraße direkt Richtung Osten auf den Breitscheitplatz ! Von der Straße des 17. Juni Richtung Süden zum Kudamm ! 
& DEMOKRATEN[ORG], Ernst-Reuther-Platz[LOC], Breitscheitplatz[LOC], Kudamm ! ![LOC]), Neue Kantstraße[LOC], Breitscheitplatz[LOC], Kudamm ![LOC]
& FU[ORG], Ernst-Reuther-Platz[LOC], Breitscheitplatz[LOC], Kudamm[LOC], Kantstraße[LOC], Breitscheitplatz[LOC], Kudamm[LOC]\\ 
\hline
6 
& Dr. Gunter Frank : Wie Moralismus dazu führt das man Feinde sieht und vernichten will — Antihygienista - Offene Gesellschaft Kurpfalz 15:04 Raus auf die Straßen   \"Ubersicht / Overview 
& Gunter Frank[PER], Antihygienista[LOC], Offene Gesellschaft Kurpfalz[ORG] 
& Gunter Frank[PER], Antihygienista[ORG], Offene Gesellschaft Kurpfalz[ORG]\\ 
\hline
7 
& Einfach nur Krank solche Politiker ! Was sollen wir mit ihm machen wenn unsere Kinder anhand dieser Impfung nen schaden erleiden ? Wer ist dann dafür verantwortlich , wen schmeißen wir dann in den Knast und nehmen sein ganzes Geld weg , sollen wir das mit dir machen ? Raus auf die Straße 
& Einfach [MISC]
& \textit{no entities detected}\\ 
\hline
8 
& 27.11.2021 Frankfurt Durchsagen von gemischt mit Musik . Super Idee   Für mehr Berichte folgen auf ... Raus auf die Straßen   \"Ubersicht / Overview 
& Frankfurt[LOC]
& Frankfurt[LOC]\\ 
\hline
9 
& Demo-Berlin : Der Mann mit dem Schild wurde soeben von der Polizei festgenommen . Circa 7 Einsatzwägen sind vor Ort und umzäunen den Park großräumig . Am Humboldthain gibt es nichts mehr zu gewinnen . \# b2908 Raus auf die Straßen 
& Der Mann mit dem Schild[LOC], Einsatzwägen[LOC], Humboldthain[LOC]
& Demo-Berlin[LOC], Humboldthain[LOC]\\ 
\hline
10 
& Anthony Fauci : Definition von `` vollständig geimpft '' könnte geändert werden — Epoch Times Nachrichten 6:59 Raus auf die Straßen   \"Ubersicht / Overview 
& Anthony Fauci[PER], Times Nachrichten[MISC]
& Anthony Fauci[PER], Epoch Times Nachrichten[ORG]

\end{tabular}
\end{adjustbox}
\end{table}

\section{Exploring the Potentials of Machine Translation for NER}

Even though the German spaCy model performed worse than the German Flair model, its English equivalent comes with one great advantage for my purpose: It supports a greater number of entity types, including date and time, making it potentially useful for monitoring protest mobilization. Furthermore, regarding the fact that English models are generally known to be more well-resourced, I decided to explore the performance of the English variant for both models. 

\subsection{The English-language spaCy Model}

The English-language spacy model supports a total of 18 entity types, including the standard entity types person, location, organization. For my aim, the entity types time and date are particularly relevant. For a detailed overview of all entity types and their definitons see table \ref{tab:spacy_ents_def} in the appendix. The underlying architecture is the same as for the German model. 

Judging by the listed data sources in the documentation, I assume that \gls{ner} model was trained on the OntoNotes5 corpus, containing data from various text genres including news, phone conversations, blogs, usenet newsgroups, etc. (cf. \cite{ontonotes5}). The model achieved an F1 score of 85.00. 

\subsection{The English-language Flair Model}
The English-language Flair model for \gls{ner} supports the same four standard entity types as the German version. Unlike the English spaCy model, it offers no advantages in terms of entity types. The architecture of the standard English-language Flair model is the same as for the standard German model. 

The English model was trained on a corrected version of the \gls{conll} 2003 corpus and achieved an F1 score of 93.06 (cf. \cite{flair_ner}). 

For implementation, the example messages were first translated using the DeepL API.\footnote{\url{https://www.deepl.com/docs-api}. For implementation details of the translation step, see: \url{https://github.com/eli-quereli/ner-german-telegram/tree/main/src/notebooks/3_exploration/translation}}. The implementation for obtaining the \gls{ner} results from both models followed the same procedure as for the German models.\footnote{For NER implementation, see \url{https://github.com/eli-quereli/ner-german-telegram/tree/main/src/notebooks/3_exploration/spacy_flair_bert_first_experiments}.}

\subsection{Results}
Table \ref{tab:detect_ents_en} shows the results of both models for the translated texts. 

We can observe that the overall performcance of the spaCy model deteriorates compared to the German version: Out of the 25 entity tags, 17 are misclassifications, e.g. the person name `Sven Liebich` as organization, or the location `Humboldthain' as person, to name just a few. Furthermore, it misses named entities which were recognized by its German equivalent, e.g. the person mentions `Haseloff' and `Elijah Tee'. The model also proves largely unsuitable for extracting dates and times: Misclassifying e.g. the date `13.06' or the time mention `16.30' as cardinals, and the postal code `15831' as date. Out of the 11 mentions of times or dates in the example data, it correctly recognizes only the sequence `03/14/2021 14032021' in message no. 3 as date. 

The performance of the English Flair model worsens as well compared to its German equivalent, although not quite as strong as the spaCy model: The model yields more false entity classifications, e.g. of `Raus auf die Straßen Overview' as organization, or `Dresdeners', `Streets Overview', `Stern!' as miscellaneous. Regarding locations, the model now misclassifies the mentions in message no. 3 as miscellaneous, which were at least partly correctly tagged by the German Flair model. Interestingly, it is the only model which partly recognizes the location mention `Straße des 17. Juni'. 

\begin{table}[htp]
\caption{spacy vs. Flair: Detected entities for English-language example messages}
\label{tab:detect_ents_en}
\centering
\begin{adjustbox}{max width=\textwidth}
\begin{tabular}{|p{0.04\linewidth}|p{0.55\linewidth}|p{0.36\linewidth}|p{0.3\linewidth}|}
\hline
\textbf{No.} & \textbf{Text} & \textbf{spaCy/en\_core\_web\_md} & \textbf{flair/ner-english} \\
\hline 
1 
& Freiburg procession with final rally at the square of the old synagogue . Live music ! Details 19062021 Out on the street
& \textit{no entities detected}
& Freiburg[LOC]\\
\hline
2 
& GO DEMONSTRATE ! - Message from Elijah Tee to the Dresdeners \& other demonstrators in Germany Tomorrow on 13.06. at 16.30 clock and nationwide everywhere , where there are fellow thinkers . - Upload pictures and videos - discussion drive or ride along 
& Germany[GPE], Tomorrow[DATE], 13.06[CARDINAL], 16.30[CARDINAL]
& Elijah Tee[PER], Dresdeners[MISC], Germany[LOC]\\ 
\hline
3 
& 15831 Blankenfelde Mahlow Berlin Speckgürtel Lichtenrade exit on 03/14/2021 14032021 Out on the road
& 15831[DATE], Blankenfelde Mahlow Berlin[PERSON], Speckgürtel Lichtenrade[PERSON], 03/14/2021 14032021[DATE] 
& "Blankenfelde Mahlow Berlin Speckgürtel Lichtenrade"[MISC]\\ 
\hline

4 
& 2021 10 11 Sven Liebich asks Prime Minister Haseloff about his understanding of assembly free - Sven Liebich 0:23 Raus auf die Straßen Overview / Overview 
& Sven Liebich[ORG], Haseloff[PER], Straßen Overview / Overview[ORG]
& Sven Liebich[PER], Haseloff[PER], Sven Liebich[PER], Raus auf die Straßen Overview[ORG]\\ 
\hline
5 
& FOR ALL DEMOCRATS DANGER : The police blocks the Stern !   That's why people run from Ernst-Reuther-Platz to the southeast to Breitscheitplatz and there further to Kudamm ! ! Join : Neue Kantstraße directly to the east to Breitscheitplatz ! From the Straße des 17. Juni southwards to the Kudamm ! 
& DEMOCRATS[NORP], Ernst-Reuther-Platz[ORG], Breitscheitplatz[PERSON], Kudamm[FAC], Neue Kantstraße[PERSON], Straße[GPE], 17[CARDINAL], Kudamm[FAC] 
& Stern ![MISC], Ernst-Reuther-Platz[LOC], Breitscheitplatz[LOC], Kudamm[LOC], Neue Kantstraße[ORG], Breitscheitplatz[LOC, ]Straße des[LOC], Juni[LOC], Kudamm[LOC]\\ 
\hline

6 
& Dr. Gunter Frank : How moralism leads to the fact that one sees enemies and wants to destroy - Antihygienista - Offene Gesellschaft Kurpfalz 15:04 Raus auf die Straßen \"Ubersicht / Overview 
& Gunter Frank[PER], 15:04 Raus auf[PER], Straßen Übersicht / Overview[ORG] 
& Gunter Frank[PER]\\ 
\hline

7 
&  "Just sick such politicians ! What should we do with him if our children suffer from this vaccination ? Who is responsible for it , who do we throw in jail and take away all his money , shall we do that with you ? Out on the street" 
& \textit{no entities detected}
& \textit{no entities detected}\\ 
\hline

8 
& 27.11.2021 Frankfurt announcements of mixed with music . Super idea For more reports follow on ... Out on the streets Overview 
& Frankfurt[GPE]
& Frankfurt[LOC]\\ 
\hline

9 
& Demo-Berlin : The man with the sign has just been arrested by the police . About 7 emergency vehicles are on site and fence the park extensively . At the Humboldthain there is nothing more to win . \# b2908 Out on the streets 
 
& Humboldthain[PER], \#[CARDINAL]
& Humboldthain[LOC]\\ 
\hline

10 
& Anthony Fauci : Definition of " fully vaccinated " could be changed - Epoch Times News 6:59 Out on the Streets Overview / Overview. 

& Anthony Fauci[PER], the Streets Overview / Overview[ORG]
& Anthony Fauci[PER], Epoch Times News[ORG], Streets Overview[MISC]
\end{tabular}
\end{adjustbox}
\end{table}

\section{Summarization}
I selected ten example messages from the Telegram channel `Demotermine', containing mobilization messages for protests against the COVID-19 measures in Germany. I applied two existing \gls{ner} models, the German-language spaCy model and the German-language Flair model and explored the named entities recognized by them. 
I then translated the example messages to English, and applied the English-language equivalents of the two models to explore the potentials of machine translation for my purpose. 

My findings indicate that the existing solutions are only of limited use for my concern: The predictions of the spaCy model contain a lot of misclassifications, both for German and English. The Flair model delivers more accurate results, with a decrease in performance for the translated messages. One reason for this could be that person or location names are often quite language area-specific, and therefore are better recognized by models trained on data from their origin language area.\\
Most importantly, most of the analyzed models are restricted to the standard entity types person, location, organization, and other/miscellaneous. This makes it impossible to extract e.g. time or date mentions which are of central relevance for monitoring COVID-19 protests. The only model which provides the entity types time and date delivers poor results and misses by far most of the date and time mentions included in the examples.
Against this background, the remaining part of my thesis explores the potentials and limitations of building  a custom solution. 
