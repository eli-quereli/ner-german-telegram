\contentsline {chapter}{Acronyms}{1}{section*.3}%
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}%
\contentsline {chapter}{\numberline {2}Named Entity Recognition: An Introduction}{8}{chapter.2}%
\contentsline {section}{\numberline {2.1}NER as a Sequence Labeling Task}{9}{section.2.1}%
\contentsline {section}{\numberline {2.2}Standard Named Entity Types}{9}{section.2.2}%
\contentsline {section}{\numberline {2.3}The BIO Tagging Scheme}{10}{section.2.3}%
\contentsline {section}{\numberline {2.4}Evaluation of NER Systems}{10}{section.2.4}%
\contentsline {subsection}{\numberline {2.4.1}F1, Precision, and Recall}{11}{subsection.2.4.1}%
\contentsline {subsection}{\numberline {2.4.2}Accuracy}{12}{subsection.2.4.2}%
\contentsline {section}{\numberline {2.5}General Challenges for NER}{13}{section.2.5}%
\contentsline {section}{\numberline {2.6}Challenges for NER in User-Generated Texts}{14}{section.2.6}%
\contentsline {chapter}{\numberline {3}Related Work}{17}{chapter.3}%
\contentsline {section}{\numberline {3.1}Shared Tasks on German-Language NER}{17}{section.3.1}%
\contentsline {section}{\numberline {3.2}Experimental Studies and Shared Tasks on Social Media NER}{18}{section.3.2}%
\contentsline {chapter}{\numberline {4}State of the Art NER with Transformer Models}{21}{chapter.4}%
\contentsline {section}{\numberline {4.1}Self-Attention}{22}{section.4.1}%
\contentsline {section}{\numberline {4.2}Key, Query, and Values}{22}{section.4.2}%
\contentsline {section}{\numberline {4.3}Multi-Head Attention}{24}{section.4.3}%
\contentsline {section}{\numberline {4.4}Transformer Blocks}{24}{section.4.4}%
\contentsline {section}{\numberline {4.5}Limited Input Length}{24}{section.4.5}%
\contentsline {section}{\numberline {4.6}Positional Embeddings}{25}{section.4.6}%
\contentsline {section}{\numberline {4.7}Model Architectures}{26}{section.4.7}%
\contentsline {section}{\numberline {4.8}Pre-Training and Fine-Tuning of Transformer Models}{26}{section.4.8}%
\contentsline {chapter}{\numberline {5}SpaCy and Flair: A Qualitative Exploration of Existing Solutions}{27}{chapter.5}%
\contentsline {section}{\numberline {5.1}Example Data}{28}{section.5.1}%
\contentsline {section}{\numberline {5.2}German-language NER with spaCy and Flair}{28}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}The German-language spaCy model}{28}{subsection.5.2.1}%
\contentsline {subsection}{\numberline {5.2.2}The German-language Flair Model}{29}{subsection.5.2.2}%
\contentsline {subsection}{\numberline {5.2.3}Results}{30}{subsection.5.2.3}%
\contentsline {section}{\numberline {5.3}Exploring the Potentials of Machine Translation for NER}{32}{section.5.3}%
\contentsline {subsection}{\numberline {5.3.1}The English-language spaCy Model}{32}{subsection.5.3.1}%
\contentsline {subsection}{\numberline {5.3.2}The English-language Flair Model}{32}{subsection.5.3.2}%
\contentsline {subsection}{\numberline {5.3.3}Results}{33}{subsection.5.3.3}%
\contentsline {section}{\numberline {5.4}Summarization}{35}{section.5.4}%
\contentsline {chapter}{\numberline {6}Building a Custom Model for NER in Telegram Channels}{36}{chapter.6}%
\contentsline {section}{\numberline {6.1}Building the Telegram Corpus}{36}{section.6.1}%
\contentsline {subsection}{\numberline {6.1.1}Raw Data}{37}{subsection.6.1.1}%
\contentsline {subsection}{\numberline {6.1.2}Text Cleaning}{37}{subsection.6.1.2}%
\contentsline {subsection}{\numberline {6.1.3}Sample Selection}{38}{subsection.6.1.3}%
\contentsline {subsection}{\numberline {6.1.4}Annotation}{39}{subsection.6.1.4}%
\contentsline {subsection}{\numberline {6.1.5}Telegram Corpus Statistics}{40}{subsection.6.1.5}%
\contentsline {section}{\numberline {6.2}The DFKI Smartdata Corpus}{40}{section.6.2}%
\contentsline {subsection}{\numberline {6.2.1}Label Mapping}{41}{subsection.6.2.1}%
\contentsline {subsection}{\numberline {6.2.2}Adjusted Smartdata Corpus Statistics}{41}{subsection.6.2.2}%
\contentsline {section}{\numberline {6.3}The Final Corpus}{42}{section.6.3}%
\contentsline {section}{\numberline {6.4}Models}{42}{section.6.4}%
\contentsline {subsection}{\numberline {6.4.1}BERT Models}{42}{subsection.6.4.1}%
\contentsline {subsubsection}{dbmdz/bert-base-german-uncased}{43}{section*.14}%
\contentsline {subsubsection}{deepset/gbert-base}{43}{section*.15}%
\contentsline {subsection}{\numberline {6.4.2}DistilBERT Model}{43}{subsection.6.4.2}%
\contentsline {subsubsection}{distilbert-base-multilingual-cased}{43}{section*.16}%
\contentsline {subsection}{\numberline {6.4.3}XLM-Roberta Models}{44}{subsection.6.4.3}%
\contentsline {subsubsection}{xlm-roberta-base}{44}{section*.17}%
\contentsline {subsubsection}{cardiffnlp/twitter-xlm-roberta-base}{44}{section*.18}%
\contentsline {section}{\numberline {6.5}Experimental setup}{45}{section.6.5}%
\contentsline {section}{\numberline {6.6}Defining a Baseline}{45}{section.6.6}%
\contentsline {section}{\numberline {6.7}Fine-tuning an NER model}{45}{section.6.7}%
\contentsline {subsection}{\numberline {6.7.1}Preprocessing: Tokenization and Encoding}{46}{subsection.6.7.1}%
\contentsline {subsubsection}{Label alignment for tokenized inputs}{46}{section*.19}%
\contentsline {subsection}{\numberline {6.7.2}Loading and Configuring a Model}{47}{subsection.6.7.2}%
\contentsline {subsection}{\numberline {6.7.3}Defining Training Arguments}{48}{subsection.6.7.3}%
\contentsline {subsubsection}{Data Collator}{48}{section*.21}%
\contentsline {subsection}{\numberline {6.7.4}Evaluation Metrics}{49}{subsection.6.7.4}%
\contentsline {subsection}{\numberline {6.7.5}Training}{49}{subsection.6.7.5}%
\contentsline {section}{\numberline {6.8}Evaluation Results}{49}{section.6.8}%
\contentsline {section}{\numberline {6.9}Error Analysis}{51}{section.6.9}%
\contentsline {subsubsection}{Token-level Errors}{51}{section*.26}%
\contentsline {subsubsection}{Class-level Errors}{52}{section*.28}%
\contentsline {subsubsection}{Confusion Matrix}{53}{section*.30}%
\contentsline {section}{\numberline {6.10}Application of the Custom Model}{54}{section.6.10}%
\contentsline {subsection}{\numberline {6.10.1}Example Messages}{55}{subsection.6.10.1}%
\contentsline {subsection}{\numberline {6.10.2}Larger Sample}{55}{subsection.6.10.2}%
\contentsline {section}{\numberline {6.11}Summarization}{57}{section.6.11}%
\contentsline {chapter}{\numberline {7}Conclusion}{60}{chapter.7}%
\contentsline {section}{\numberline {7.1}Summarization}{60}{section.7.1}%
\contentsline {section}{\numberline {7.2}Discussion}{62}{section.7.2}%
\contentsline {section}{\numberline {7.3}Future Work}{63}{section.7.3}%
\contentsline {chapter}{Bibliography}{71}{chapter*.35}%
\contentsline {chapter}{Appendix}{71}{chapter*.35}%
\contentsline {chapter}{\numberline {A}Additional Material}{75}{appendix.A}%
